Il voto elettronico è possibile?

# Il voto elettronico è possibile?

Nella comunità scientifica c'è:

 > [...] un forte scetticismo sulla possibilità tecnica di garantire livelli adeguati di sicurezza
 > Andrea Caccia, esperto di blockchain e presidente della commissione UNI/CT 532

Se ti dicessero che molti esperti credono che, sì, si può volare con un aereo ma è troppo pericoloso ed è probabile che ti schianti a terra. Oppure ancora che se fare un aereo con dei livelli di sicurezza adeguati costasse talmente tanto che sarebbe più economico e comunque sicuro andare in treno o in autobus?

Se il mondo andasse veramente così sono certo che nessuno metterebbe piede in un aereo. Eppure quando si parla di voto elettronico le cose non sembrano andare allo stesso modo... 

![La vignetta in questione](https://imgs.xkcd.com/comics/voting_software.png)

La vignetta esagerando mostra il "pessimismo" - sarebbe meglio parlare di realismo - di un esperto in sicurezza informatica che viene contrattoposto all' "ingenuità" di un 'intervistatrice per nulla convinta del reale pericolo rappresentato dal voto elettronico. È, infatti, far percepire questo genere di pericoli che sembrano così distanti e difficili da capire. 

Quando si parla di aerei comprendiamo i grossi rischi e fatichiamo ad accettarli anche quando il bassissimo tasso di incidenti rispetto al numero di voli totale ci dice che volare è sicuro ed è un rischio più che accettabile rispetto al rischio che corriamo, per esempio, quando saliamo a bordo di un'auto. Invece quando si parla di voto elettronico nonostante le possibilità di disastri enormi, come la rilevazione di un broglio che costringerebbe a reindire le elezioni annullando le precedenti, le persone e i giornali non sembrano dare l'attenzione che meriterebbe data l'importanza della questione. Anche nonostante i numerosi e documentati problemi che sono stati rilevati e potete trovare in [questa pagina di Wikipedia.](https://en.wikipedia.org/wiki/Electronic_voting#Documented_problems_with_electronic_voting)

Credo che la cosidetta "opinione pubblica" dovrebbe essere quindi molto attenta alla sicurezza dei sistemi di voto elettronico quando sono usati dallo Stato per delle elezioni o da un partito.


Vi consiglio quindi di leggere [il seguente articolo](https://copernicani.it/Libro_bianco/il-voto-elettronico-sicurezza-e-privacy-al-sicuro/) per approfondire le tematiche di questo post. Se invece volete qualcosa di più tecnico e approfondito vi consiglio il report dell'ENISA (European Union Agency for Network and Information Security) ["ENISA makes recommendations on EU-wide election cybersecurity"](https://www.enisa.europa.eu/news/enisa-news/enisa-makes-recommendations-on-eu-wide-election-cybersecurity)

# Note

**Full disclosure**: Questo è un blog che non vuole fare divulgazione in modo troppo impegnativo, per gli altri ma sopratutto per me. Non ho quindi intenzione di mettermi a fare metaricerche e confrontare tutti i paper scientifici come dovrebbe fare un divulgatore o un giornalista serio: non sono pagato per fare questo e mi ci vorrebbe un sacco di tempo. Quindi a supporto di quello che scrivo linkerò uno o qualche articolo, da fonti che ritengono affidabili o opioni di persone che ritengo esperte. In questo caso invece un divulgatore serio dovrebbe informarsi per capire l'opinione della comunità scientifica e riportare quella dando spazio magari anche all'opinione del singolo esperto. 

Scrivo queste note così prendete "cum grano salis" quello che scrivo e fate attenzione, più che alle mie tesi, alle questioni che porto in evidenza.

## Note per gli elettori e iscritti al M5S

Sto evitando di mantenere il focus del blog sulla tecnologia e non politica anche se, devo dire, è veramente difficile quando si tratta di determinate questioni. 

 > L'informatica è la realizzazione di una visione del mondo.
 > W. Vannini

Scrivendo di voto elettronico non riesco a non citare almeno la multa e il report del garante della Privacy italiano riguardo al sistema di voto elettronico Rousseau. Se uno dei partiti di governo in italia prende decisioni importantissime basate su un sistema di voto che il garante ha definito, riassumendo, non sicuro e non segreto, è sicuramente *un grande problema*. Non commenterò oltre ma esorto gli elettori o iscritti al partito, di fare pressione per mettere al centro delle discussioni, almeno interne, il problema.


### Approfondimento sulla questione Rousseau

Vi consiglio di leggere questo [articolo di Techeconomy che raccoglie pareri di esperti](https://www.techeconomy.it/2019/04/06/di-rousseau-data-breach-e-voto-elettronico/).


#### FONTI

https://www.enisa.europa.eu/news/enisa-news/enisa-makes-recommendations-on-eu-wide-election-cybersecurity